<?php

namespace App\Models;

class VisitorLogs extends AAAModel
{
    public function visitor()
    {
        return $this->belongsTo(Visitors::class, 'visitor_fk', 'id');
    }
}
