<?php

namespace App\Models;

use App\Util;
use Illuminate\Support\Str;

class File extends AAAModel
{
    const IMAGES_FORMAT = ["jpg", "png", "jpeg"];

    public static function create($file)
    {
        // initialize file meta data
        $fileInput = [
            "name" => $file->getClientOriginalName(),
            "extension" => $file->getClientOriginalExtension(),
            "size" => Util::formatBytes($file->getSize()),
            "sizeRaw" => $file->getSize()
        ];

        // validate: file size
        if ($fileInput["sizeRaw"] > 2097152) {
            throw new \Exception("failed, file size greater than 2 MB");
        }

        // validate: file format
        $validExtensions = File::IMAGES_FORMAT;
        if (!in_array($fileInput["extension"], $validExtensions)) {
            throw new \Exception("file format " . $fileInput["extension"] . " not allowed");
        }

        // mapping data into the model
        $model = new File();
        $model->file_path = $file->store("upload");
        $model->file_size = $fileInput["size"];
        $model->file_name = $fileInput["name"];
        $model->file_keychar = File::generateFileKeychar();
        $model->save();

        return $model->file_keychar;
    }

    public static function generateFileKeychar()
    {
        $key = Str::random(25);
        $model = self::where(["file_keychar" => $key])->first();
        if (!$model) {
            return $key;
        }
        return self::generateFileKeychar();
    }

    public static function getFileIdByKeychar($keychar)
    {
        $keychar = str_replace("\"", "", $keychar);
        $model = self::where(["file_keychar" => $keychar])->first();
        if (!$model) {
            return null;
        }
        return $model->id;
    }

    public static function getFileUrlByKeychar($keychar)
    {
        $model = self::where("file_keychar", $keychar)->first();
        if (!$model) {
            return null;
        }
        return url($model->file_path);
    }

    public static function getFileUrlById($id)
    {
        $model = self::where("id", $id)->first();
        if (!$model) {
            return null;
        }
        return url($model->file_path);
    }

    public static function isImageFile($fileName)
    {
        $fileName = explode(".", $fileName);
        if (in_array($fileName[1], self::IMAGES_FORMAT)) {
            return true;
        }
        return false;
    }
}
