<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AAAModel extends Model
{
  protected $casts = [
    "deleted_at" => 'datetime:Y-m-d H:m:s',
    "updated_at" => 'datetime:Y-m-d H:m:s',
    "created_at" => 'datetime:Y-m-d H:m:s',
  ];
}
