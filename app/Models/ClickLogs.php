<?php

namespace App\Models;

class ClickLogs extends AAAModel
{
    public function visitor()
    {
        return $this->belongsTo(Visitors::class, 'visitor_fk', 'id');
    }
}
