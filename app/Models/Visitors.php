<?php

namespace App\Models;

use App\Util;

class Visitors extends AAAModel
{
    public static function generateVisitorId()
    {
        return "VST-" . Util::generateUniqueID(
            model: self::class,
            field: "visitor_id",
            length: 6,
            numeric: true
        );
    }
}
