<?php

namespace App;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Util
{
    public static function formatting($datas, $callback)
    {
        try {
            foreach ($datas as $index => $data) {
                $datas[$index] = $callback($data);
            }
        } catch (\Exception $e) {
            $datas = $callback($datas);
        }
        return $datas;
    }

    public static function createSlug($string, $separator = '-')
    {
        $slug = strtolower(trim($string));
        $slug = preg_replace('/[^a-z0-9-]+/', $separator, $slug);
        $slug = preg_replace('/-+/', $separator, $slug);
        return $slug;
    }

    public static function mergeAndRemoveItems($arrays, $itemsToRemove)
    {
        $mergedArray = call_user_func_array('array_merge', $arrays);
        $mergedArray = array_diff($mergedArray, $itemsToRemove);
        return $mergedArray;
    }

    public static function generateRandomAlphaNumericString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomNumericString($length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function isValidGoogleMapsLink($param)
    {
        return strpos($param, "https://www.google.com/maps/embed");
    }

    public static function batchDefaultValidation($models, $params, $id = false)
    {
        $isNotValid = false;
        foreach ($models as $modelClass) {
            if (!class_exists($modelClass)) {
                continue;
            }

            if (method_exists($modelClass, 'defaultValidation')) {
                $isNotValid = $modelClass::defaultValidation($params, $id);
                if ($isNotValid) {
                    return $isNotValid;
                }
            } else {
                continue;
            }
        }
        return $isNotValid;
    }

    public static function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');
        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)] . 'B';
    }

    public static function getJWTPayload()
    {
        $headers = getallheaders();
        try {
            if (isset($headers["Authorization"])) {
                $clientToken = $headers["Authorization"];
            } else if (isset($headers["authorization"])) {
                $clientToken = $headers["authorization"];
            }
            $clientToken = explode(" ", $clientToken)[1];
            $payload = JWT::decode($clientToken, new Key(env("JWT_SECRET"), 'HS256'));
            return $payload;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getRoleLogin()
    {
        $jwtPayload = self::getJWTPayload();
        return self::getArrOrObject($jwtPayload, "login_role");
    }

    public static function isAdmin()
    {
        $currentRole = self::getRoleLogin();
        return $currentRole == "admin";
    }

    public static function getSessionId()
    {
        $jwtPayload = self::getJWTPayload();
        return self::getArrOrObject($jwtPayload, "session_id");
    }

    public static function getUserId()
    {
        $jwtPayload = self::getJWTPayload();
        return self::getArrOrObject($jwtPayload, "login_id");
    }

    public static function getEmail()
    {
        $jwtPayload = self::getJWTPayload();
        return self::getArrOrObject($jwtPayload, "login_email");
    }

    public static function formatExcel($spreadsheet)
    {
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            $spreadsheet->setActiveSheetIndex($spreadsheet->getIndex($worksheet));
            $sheet = $spreadsheet->getActiveSheet();
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) {
                $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            }
        }
        return $spreadsheet;
    }

    public static function getNextSequence($tableName)
    {
        $id = DB::select("SHOW TABLE STATUS LIKE '$tableName'");
        $nextId = $id[0]->Auto_increment;
        return $nextId;
    }

    public static function goOutJson($data, $responseCode = 500)
    {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($responseCode);
        $data["status_code"] = $responseCode;
        echo json_encode($data);
        exit();
    }

    public static function unauthorizedResult($message = false)
    {
        if (!$message) {
            $message = "kamu tidak memiliki akses";
        }
        return self::defaultResult($message, 401);
    }

    public static function debugResult($data, $simpleMode = false)
    {
        $statusCode = 400;
        $apiResponse = [
            "message" => "fitur ini sedang dalam proses perbaikan",
            "status_code" => $statusCode
        ];
        if ($simpleMode) {
            $apiResponse["debug"] = [
                "data" => $data,
                "data_type" => gettype($data)
            ];
        } else {
            $apiResponse = array_merge($apiResponse, $data);
        }
        return response($apiResponse, $statusCode);
    }

    public static function defaultResult(
        $message = "",
        $statusCode = 200,
        $data = false,
        $additionalData = false,
        $rawResult = false,
        $isError = false,
        $error = false,
        $redirect = false,
    ) {
        if ($isError) {
            $message = "terjadi kesalahan pada sistem";
            $statusCode = 400;
        }
        if ($error and $isError) {
            if (!config('constant.IS_PROD')) {
                $data = $error;
            }
        }
        $apiResponse = [
            "message" => $message,
            "status_code" => $statusCode,
            "status" => ($statusCode == 200) ? true : false,
        ];

        if ($data) {
            $apiResponse["data"] = $data;
        } else {
            if (is_array($data)) {
                $apiResponse["data"] = [];
            }
        }

        if (!config('constant.IS_PROD')) {
            $apiResponse["dev_mode"] = true;
        }

        if ($additionalData) {
            $apiResponse = array_merge($additionalData, $apiResponse);
        }

        if ($rawResult) {
            return $apiResponse;
        }

        if ($redirect) {
            if ($statusCode == 200) {
                $status = "success";
            } else {
                $status = "error";
            }
            $response = $apiResponse;
            $response[$status] = $message;
            return redirect($redirect)->with($response);
        }

        return response(
            $apiResponse,
            $statusCode
        );
    }

    public static function isset($arrayOrObject, $key)
    {
        $param = self::getArrOrObject($arrayOrObject, $key);
        if ($param) {
            return isset($param) && ($param != '') && ($param != null) && ($param != "null");
        }
        return false;
    }

    public static function getArrOrObject($arrayOrObject, $key, $default = null)
    {
        $res = $default;
        if (is_array($arrayOrObject)) {
            $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
            if ($res == null) {
                $key = strtoupper($key);
                $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
                if ($res == null) {
                    $key = strtolower($key);
                    $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
                }
            }
        } else {
            if (is_object($arrayOrObject)) {
                if (property_exists($arrayOrObject, $key) || isset($arrayOrObject->$key)) {
                    $res = $arrayOrObject->$key;
                }
            }
        }
        if ($res == 'undefined') {
            return $default;
        }
        return $res;
    }

    public static function paginationBuilder($model, $params, $searchFields = [], $customCallback = false, $defaultLength = 10)
    {
        $dataPagination = [];
        $start = isset($params["start"]) ? (int) $params["start"] : 0;
        $length = isset($params["length"]) ? (int) $params["length"] : $defaultLength;
        $searchParam = isset($params["search"]["value"]) ? $params["search"]["value"] : false;
        $searchParam = isset($params["search"]) ? $params["search"] : false;

        if ($searchParam) {
            if (count($searchFields) != 0) {
                if (count($searchFields) == 1) {
                    $dataPagination = $model->where($searchFields[0], "LIKE", "%$searchParam%");
                } else {
                    $dataPagination = $model->where($searchFields[0], "LIKE", "%$searchParam%");
                    for ($i = 0; $i < count($searchFields); $i++) {
                        if ($i >= 1) {
                            $dataPagination->orWhere($searchFields[$i], "LIKE", "%$searchParam%");
                        }
                    }
                }
            }
            $dataPagination->limit($length)->offset($start);
            $recordsTotal = $dataPagination->count();
            $dataPagination = $dataPagination->get();
        } else {
            $dataPagination = $model->limit($length)->offset($start);
            $recordsTotal = $dataPagination->count();
            $dataPagination = $dataPagination->get();
        }

        if ($customCallback) {
            $dataPagination = call_user_func($model, $customCallback);
        }

        $apiResponse = [
            "data" => $dataPagination,
            "start" => $start,
            "length" => $length,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsTotal,
            "status_code" => 200
        ];

        return response($apiResponse, 200);
    }

    public static function validationRequiredParams(
        $requiredParams,
        $params,
        $paramAliases = false,
        $redirect = false
    ) {
        foreach ($requiredParams as $param) {
            if (self::isset($params, $param)) {
                if ($params[$param] == null) {
                    if ($paramAliases) {
                        $paramName = $paramAliases[$param];
                    } else {
                        $paramName = str_replace("_", " ", $param);
                    }
                    return self::defaultResult(
                        message: "$paramName is required",
                        statusCode: 400,
                        data: false,
                        additionalData: false,
                        redirect: $redirect
                    );
                }
            } else {
                $paramName = str_replace("_", " ", $param);
                if ($paramAliases) {
                    $paramName = $paramAliases[$param];
                }
                return self::defaultResult(
                    message: "$paramName is required",
                    statusCode: 400,
                    data: false,
                    additionalData: false,
                    redirect: $redirect
                );
            }
        }
    }

    public static function isAvailable($model, $field, $param, $id = false, $isString = true)
    {
        if ($isString) {
            $param = strtolower($param);
            $field = DB::raw("LOWER($field)");
        }
        $data = $model::where($field, $param)->whereNull('deleted_at');

        if ($id) {
            $data = $data->where("id", "!=", $id);
        }
        $data = $data->first();
        if (!$data) {
            return true;
        }
        return false;
    }

    public static function generateUniqueID($model, $field, $length = 12, $numeric = false)
    {
        if ($numeric) {
            $uid = self::generateRandomNumericString($length);
        } else {
            $uid = Str::random($length);
        }
        $model = $model::where([$field => $uid])->first();
        if (!$model) {
            return $uid;
        }
        return self::generateUniqueID($model, $field);
    }

    public static function formatRupiah($value)
    {
        $result = "Rp." . number_format($value, 0, ',', '.');
        return $result;
    }

    public static function formatNumber($value)
    {
        $result = number_format($value, 0, ',', '.');
        return $result;
    }

    public static function in_2array($needle, $haystack, $full_search = false)
    {
        $ret = false;
        foreach ($needle as $search) {
            if (in_array($search, $haystack)) {
                $ret = true;
                if (!$full_search) {
                    break;
                }
            } else if ($full_search) {
                $ret = false;
                break;
            }
        }
        return $ret;
    }

    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];

    public static function getMonthName($index)
    {
        return self::getArrOrObject(self::months, $index - 1, "-");
    }

    public static function preprocessNullString($params)
    {
        foreach ($params as &$one) {
            if ($one == "null") {
                $one = null;
            }
        }
        return $params;
    }

    public static function renameParams($params, $aliases)
    {
        $aliasKeys = array_keys($aliases);
        foreach ($aliasKeys as $oldKey) {
            if (isset($params[$oldKey])) {
                $newKey = $aliases[$oldKey];
                $params[$newKey] = $params[$oldKey];
                unset($params[$oldKey]);
            }
        }
        return $params;
    }

    public static function getCsrfToken()
    {
        return self::getArrOrObject($_COOKIE, "XSRF-TOKEN");
    }

    public static function getClientIp()
    {
        $headers = getallheaders();
        return self::getArrOrObject($headers, "X-Real-IP", null);
    }

    public static function isStrongPassword($password)
    {
        // Minimum length is 8 characters
        if (strlen($password) < 8) {
            return false;
        }

        // At least one uppercase letter
        if (!preg_match('/[A-Z]/', $password)) {
            return false;
        }

        // At least one lowercase letter
        if (!preg_match('/[a-z]/', $password)) {
            return false;
        }

        // At least one number
        if (!preg_match('/[0-9]/', $password)) {
            return false;
        }

        // If all criteria are met, the password is considered strong
        return true;
    }

    public static function isContainString($haystack, $needle)
    {
        if (is_array($needle)) {
            foreach ($needle as $substring) {
                if (strpos($haystack, $substring) !== false) {
                    return true; // If any substring is found, return true
                }
            }
        } else {
            if (strpos($haystack, $needle) !== false) {
                return true;
            }
        }
        return false; // If no substring is found, return false
    }
}
