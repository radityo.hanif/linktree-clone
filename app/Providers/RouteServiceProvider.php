<?php

namespace App\Providers;

use App\Util;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }

    protected function logAccess($request, $userId = '', $sessionId = '')
    {
        Log::info(">>>>>>>> Start Log Access >>>>>>>>");
        Log::info("Request Routes: " . $request->url());
        Log::info("Request IP: " . $request->ip());
        Log::info("User ID: " . $userId);
        Log::info("Session ID: " . $sessionId);
        Log::info(">>>>>>>> End Log Access >>>>>>>>");
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('by_session_id', function (Request $request) {
            $sessionId = Util::getSessionId();
            $userId = Util::getUserId();
            $this->logAccess(
                request: $request,
                userId: $userId,
                sessionId: $sessionId
            );
            return Limit::perMinute(env('RATE_LIMIT_BY_SESSION_ID', 60))->by($sessionId)->response(function () {
                return Util::defaultResult('You have reached your access limit. Please try after 1 minute.', 429);
            });
        });

        RateLimiter::for('public', function (Request $request) {
            $xsrfToken = $request->cookie('XSRF-TOKEN');
            $this->logAccess(
                request: $request
            );
            return Limit::perMinute(env('RATE_LIMIT_PUBLIC', 60))->by($xsrfToken)->response(function () {
                return Util::defaultResult('You have reached your access limit. Please try after 1 minute.', 429);
            });
        });
    }
}
