<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Label;
use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Visitors;
use App\Util;
use Torann\GeoIP\Facades\GeoIP;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        // gathering label data
        $labels = Label::whereNull("deleted_at")->get();
        $labels = Util::formatting(
            datas: $labels,
            callback: function ($data) {
                return [
                    "title" => Util::getArrOrObject($data, "title"),
                    "title_color" => Util::getArrOrObject($data, "title_color"),
                    "slug" => Util::getArrOrObject($data, "slug"),
                    "description" => Util::getArrOrObject($data, "description"),
                    "icon" => File::getFileUrlByKeychar(Util::getArrOrObject($data, "file_image_keychar")),
                ];
            }
        );

        // gathering userInformation
        $ipAddress = $request->ip();

        try {
            // check whether there are already visitors with this IP in the database
            $visitor = Visitors::where('ip_address', $ipAddress)->first();
            if (!$visitor) {
                // if not then create new data for this visitor
                $newVisitor = new Visitors();
                $newVisitor->visitor_id = Visitors::generateVisitorId();
                $newVisitor->ip_address = $ipAddress;
                $newVisitor->country_of_origin = GeoIP::getLocation($ipAddress)->country;
                $newVisitor->save();
            }
        } catch (\Exception $e) {
        }

        // choose main theme
        $mainTheme = 'landing.theme-1';
        $themeSetting = Setting::where([
            'key' => 'main_theme',
        ])->first();
        if ($themeSetting) {
            switch (Util::getArrOrObject($themeSetting, "value")) {
                case "theme_1":
                    $mainTheme = "landing.theme-1";
                    break;
                case "theme_2":
                    $mainTheme = "landing.theme-2";
                    break;
                default:
                    $mainTheme = "landing.theme-1";
            }
        }
        return view($mainTheme)->with([
            "labels" => $labels
        ]);
    }
}
