<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function update(Request $request)
    {
        // validate parameters
        $defaultRedirect = "/admin/dashboard/themes/";
        $params = $request->all();
        $requiredParams = [
            "key",
            "value",
        ];
        $isNotValid = Util::validationRequiredParams(
            requiredParams: $requiredParams,
            params: $params,
            redirect: $defaultRedirect
        );
        if ($isNotValid) {
            return $isNotValid;
        }

        // save model
        DB::beginTransaction();
        try {
            // check whether the settings with the associated key are valid
            if (!in_array(Util::getArrOrObject($params, "key"), Setting::validKey)) {
                throw new \Exception("given setting key is not valid");
            }
            // check whether the setting with the associated key already exists before
            $setting = Setting::where([
                "key" => Util::getArrOrObject($params, "key")
            ])->first();

            // if not found
            if (!$setting) {
                // then create a new rule
                $newSetting = new Setting();
                $newSetting->key = Util::getArrOrObject($params, "key");
                $newSetting->value = Util::getArrOrObject($params, "value");
                $newSetting->save();
            }

            // if found, update value
            if ($setting) {
                $setting->value = Util::getArrOrObject($params, "value");
                $setting->save();
            }

            DB::commit();
            return Util::defaultResult(
                message: "Success change setting",
                statusCode: 200,
                redirect: $defaultRedirect
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return Util::defaultResult(
                message: $e->getMessage(),
                statusCode: 400,
                redirect: $defaultRedirect
            );
        }
    }

    public function themes()
    {
        $themeSetting = Setting::where([
            'key' => 'main_theme',
        ])->first();
        $currentTheme = Util::getArrOrObject($themeSetting, "value");
        return view('dashboard.themes.index')->with([
            "value" => $currentTheme
        ]);
    }
}
