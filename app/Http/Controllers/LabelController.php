<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Label;
use App\Util;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LabelController extends Controller
{
    public function read()
    {
        return Util::defaultResult(
            message: "success get data",
            statusCode: 200,
            data: Label::whereNull("deleted_at")->get()
        );
    }

    public function delete(Request $request)
    {
        // validate parameters
        $params = $request->all();
        $requiredParams = [
            "id",
        ];
        $isNotValid = Util::validationRequiredParams(
            requiredParams: $requiredParams,
            params: $params,
        );
        if ($isNotValid) {
            return $isNotValid;
        }

        // start db transaction
        $id = Util::getArrOrObject($params, "id");
        $model = Label::find($id);
        if (!$model) {
            return Util::defaultResult(
                message: "data not found",
                statusCode: 400
            );
        }

        $model->deleted_at = Carbon::now();
        $model->save();
        return Util::defaultResult("success delete data", 200);
    }

    public function create(Request $request)
    {
        // validate parameters
        $defaultRedirect = "/admin/dashboard/label/create";
        $params = $request->all();
        $requiredParams = [
            "name",
            "description",
            "icon",
        ];
        $isNotValid = Util::validationRequiredParams(
            requiredParams: $requiredParams,
            params: $params,
            redirect: $defaultRedirect
        );
        if ($isNotValid) {
            return $isNotValid;
        }

        DB::beginTransaction();
        try {
            // save file
            $file = $request->file("icon");
            $uploadedFileKeychar = File::create($file);

            // save label
            $label = new Label();
            $label->title = Util::getArrOrObject($params, "name");
            $label->slug = Util::createSlug(
                string: Util::getArrOrObject($params, "name"),
                separator: "_"
            );
            $label->description = Util::getArrOrObject($params, "description");
            $label->file_image_keychar = $uploadedFileKeychar;
            $label->title_color = Util::getArrOrObject($params, "title_color");
            $label->save();

            DB::commit();
            return Util::defaultResult(
                message: "Success add new data",
                statusCode: 200,
                redirect: "/admin/dashboard/label"
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return Util::defaultResult(
                message: $e->getMessage(),
                statusCode: 400,
                redirect: $defaultRedirect
            );
        }
    }
}
