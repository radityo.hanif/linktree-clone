<?php

namespace App\Http\Controllers;

use App\Util;
use App\Models\SetupData;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class SetupDataController extends Controller
{
    public function init()
    {
        if (SetupData::all()->count()) {
            return Util::defaultResult("Data setup has been done", 400);
        }

        DB::beginTransaction();
        try {
            // provide a sign that the data setup has been carried out
            $setupData = new SetupData();
            $setupData->save();

            // setup Admin Account
            $adminEmail = env('ADMIN_DEFAULT_EMAIL', 'admin_default@yopmail.com');
            $adminPassword = env('ADMIN_DEFAULT_PASSWORD', 'password');
            $adminUser = new User();
            $adminUser->name = "Default Admin";
            $adminUser->email = $adminEmail;
            $adminUser->password = Hash::make($adminPassword);
            $adminUser->role = 'admin';
            $adminUser->status = 1;
            $adminUser->save();
            DB::commit();
            return Util::defaultResult("Successfully setup data", 200);
        } catch (\Exception $e) {
            DB::rollback();
            $apiResponse = [
                "message" => "an error occurred in the application",
            ];
            if (!config('constant.IS_PROD')) {
                $apiResponse["error"] = $e->getMessage();
            }
            return Util::defaultResult($apiResponse, 500);
        }
    }

    public function getEnv()
    {
        return Util::defaultResult(
            message: __FUNCTION__,
            statusCode: 200,
            data: [
                'apps_env' => env('APP_ENV')
            ]
        );
    }
}
