<?php

namespace App\Http\Controllers;

use App\Util;
use App\Models\Visitors;
use Illuminate\Http\Request;

class VisitorsController extends Controller
{
    public function read(Request $request)
    {
        $data = Visitors::get();
        return Util::defaultResult(
            message: "success get data",
            statusCode: 200,
            data: $data
        );
    }
}
