<?php

namespace App\Http\Controllers;

use App\Util;
use App\Models\ClickLogs;
use Illuminate\Http\Request;

class ClickLogsController extends Controller
{
    public function read(Request $request)
    {
        $data = ClickLogs::with(['visitor'])->get();
        return Util::defaultResult(
            message: "success get data",
            statusCode: 200,
            data: $data
        );
    }

    public function pieChart(Request $request)
    {
        $logs = ClickLogs::selectRaw('count(1) as value, button_name')
            ->groupBy('button_name')
            ->get();
        $labels = [];
        $values = [];
        if ($logs) {
            foreach ($logs as $log) {
                $labels[] = Util::getArrOrObject($log, "button_name");
                $values[] = Util::getArrOrObject($log, "value");
            }
        }

        $data = [
            "labels" => $labels,
            "values" => $values,
        ];

        return Util::defaultResult(
            message: "success get data",
            statusCode: 200,
            additionalData: $data
        );
    }
}
