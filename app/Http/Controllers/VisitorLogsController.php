<?php

namespace App\Http\Controllers;

use App\Models\ClickLogs;
use App\Util;
use Torann\GeoIP\Facades\GeoIP;
use App\Models\VisitorLogs;
use App\Models\User;
use App\Models\Visitors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitorLogsController extends Controller
{
    protected $sampleParam = [
        "click_logs" => [
            "button_x",
            "button_as",
            "button_245", // button id
        ],
        "time_spent" => "252" // second 
    ];

    public function create(Request $request)
    {
        // this API function will be called before the page is closed
        // validate parameters
        $params = $request->all();
        $requiredParams = [
            "click_logs",
            "time_spent",
        ];
        $isNotValid = Util::validationRequiredParams(
            requiredParams: $requiredParams,
            params: $params,
        );
        if ($isNotValid) {
            return $isNotValid;
        }

        // start db transaction
        $ipAddress = $request->ip();

        DB::beginTransaction();
        try {
            // check whether there are already visitors with this IP in the database
            $visitor = Visitors::where('ip_address', $ipAddress)->first();
            if (!$visitor) {
                // if not then create new data for this visitor
                $newVisitor = new Visitors();
                $newVisitor->visitor_id = Visitors::generateVisitorId();
                $newVisitor->ip_address = $ipAddress;
                $newVisitor->country_of_origin = GeoIP::getLocation($ipAddress)->country;
                $newVisitor->save();
                $visitor = $newVisitor;
            }

            // visitor logs
            $visitorLog = new VisitorLogs();
            $visitorLog->visitor_fk = $visitor->id;
            $visitorLog->time_spent = Util::getArrOrObject($params, "time_spent");
            $visitorLog->device_information =  User::getDeviceInformation(userAgent: Util::getArrOrObject($_SERVER, "HTTP_USER_AGENT"));
            $visitorLog->browser_information =  User::getBrowserInformation(userAgent: Util::getArrOrObject($_SERVER, "HTTP_USER_AGENT"));
            $visitorLog->save();

            // click logs
            $clickLogs = json_decode(Util::getArrOrObject($params, "click_logs"));
            if ($clickLogs) {
                if (count($clickLogs)) {
                    foreach ($clickLogs as $one) {
                        $clickLog = new ClickLogs();
                        $clickLog->visitor_fk = $visitor->id;
                        $clickLog->button_name = $one;
                        $clickLog->save();
                    }
                }
            }

            DB::commit();
            return Util::defaultResult(
                message: "ok success log visitor data",
                statusCode: 200
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return Util::defaultResult(
                message: "an error ocurred",
                data: [
                    "error" => $e->getMessage()
                ],
                statusCode: 400
            );
        }
    }

    public function read(Request $request)
    {
        $data = VisitorLogs::with(['visitor'])->get();
        return Util::defaultResult(
            message: "success get data",
            statusCode: 200,
            data: $data
        );
    }
}
