<?php

namespace App\Http\Controllers;

use App\Models\Visitors;

class DashboardController extends Controller
{
    public function index()
    {
        $visitors = Visitors::count();
        $statistics = [
            [
                'key' => 'Visitor',
                'value' => $visitors
            ]
        ];
        return view('dashboard.index')->with([
            'statistics' => $statistics
        ]);
    }
}
