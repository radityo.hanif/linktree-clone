<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Util;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        // validate parameters
        $defaultRedirect = "/admin/login";
        $params = $request->all();
        $requiredParams = [
            "email",
            "password",
        ];

        $isNotValid = Util::validationRequiredParams(
            requiredParams: $requiredParams,
            params: $params,
            redirect: $defaultRedirect
        );
        if ($isNotValid) {
            return $isNotValid;
        }

        $defaultFailResult = Util::defaultResult(
            message: "Login failed, please check the email and password again",
            statusCode: 401,
            redirect: $defaultRedirect
        );

        // check whether user data exists based on email
        $email = Util::getArrOrObject($params, "email");
        $model = User::where("email", $email)->first();
        if (!$model) {
            return $defaultFailResult;
        }

        // check password
        $isPassword = Hash::check(
            Util::getArrOrObject($params, "password"),
            Util::getArrOrObject($model, "password")
        );
        if (!$isPassword) {
            return $defaultFailResult;
        }

        // check whether the user is active
        if (Util::getArrOrObject($model, "status") == 0) {
            $email = Util::getArrOrObject($model, "email");
            return Util::defaultResult(
                message: "The account is not yet active, please check email $email to activate the account",
                statusCode: 401,
                redirect: $defaultRedirect
            );
        }

        // login successfully, generate session
        $user = [
            "login_name" => Util::getArrOrObject($model, "name"),
            "login_email" => Util::getArrOrObject($model, "email"),
            "login_id" => Util::getArrOrObject($model, "id"),
            "login_role" => Util::getArrOrObject($model, "role"),
        ];
        $request->session()->push("user", $user);
        return Util::defaultResult(
            message: "Login successfully, please welcome " . Util::getArrOrObject($model, "name"),
            statusCode: 200,
            redirect: "/admin/dashboard"
        );
    }
}
