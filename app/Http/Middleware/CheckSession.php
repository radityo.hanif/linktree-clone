<?php

namespace App\Http\Middleware;

use App\Util;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!$request->session()->has('user')) {
            return Util::defaultResult(
                message: "Your session has expired, please log in again",
                statusCode: 401,
                redirect: "/admin/login"
            );
        }
        return $next($request);
    }
}
