@php

    $sidebar = [
        [
            'title' => 'ANALYTICS',
            'data' => [
                [
                    'name' => 'Dashboard',
                    'href' => '/admin/dashboard',
                ],
            ],
        ],
        [
            'title' => 'CONTENT',
            'data' => [
                [
                    'name' => 'Labels',
                    'href' => '/admin/dashboard/label',
                ],
                [
                    'name' => 'Themes',
                    'href' => '/admin/dashboard/themes',
                ],
            ],
        ],
        [
            'title' => 'SETTINGS',
            'data' => [
                [
                    'name' => 'Logout',
                    'href' => '/admin/logout',
                ],
                [
                    'name' => 'View Website',
                    'href' => '/',
                ],
            ],
        ],
    ];

@endphp

<aside
    class="flex-col w-[20%] min-h-[100vh] px-5 py-8 overflow-y-auto bg-gradient-to-b from-blue-500 to-blue-700 rounded-r-md">
    <div class="grid gap-0">
        <h1 class="text-white text-2xl text-center">
            Smartgenix
        </h1>
    </div>
    <hr class="my-5">
    <div class="flex flex-col justify-between flex-1 mt-6">
        <nav class="-mx-3 space-y-6 ">
            @foreach ($sidebar as $item)
                <div class="space-y-3 ">
                    <label class="px-3 text-xs text-white font-bold uppercase"
                        style="letter-spacing: 1.5px">{{ $item['title'] }}</label>
                    @foreach ($item['data'] as $menu)
                        <a class="flex items-center px-3 py-2 text-gray-300 transition-colors duration-300 transform rounded-lg hover:text-blue-500 hover:bg-white"
                            href="{{ $menu['href'] }}">
                            <span class="mx-2 text-sm font-medium">{{ $menu['name'] }}</span>
                        </a>
                    @endforeach
                </div>
            @endforeach
        </nav>
    </div>
</aside>
