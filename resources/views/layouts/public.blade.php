<!doctype html>
<html data-theme="light">

@php
    $version = 200;
@endphp

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/index.css?v={{ $version }}">
    <title>Smartgenix - Genuinely Innovative Company within its Industry</title>
    <link rel="shortcut icon" href="/img/logo.png?v={{ $version }}" type="image/x-icon">
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    @vite('resources/css/app.css')
</head>

<body>
    @yield('content')
    <script src="/js/log_visitor.js?v={{ $version }}"></script>
    <script src="/js/responsive.js?v={{ $version }}"></script>
</body>

</html>
