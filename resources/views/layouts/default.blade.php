<!doctype html>
<html data-theme="light">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/index.css">
    <title>Smartgenix - Genuinely Innovative Company within its Industry</title>
    <link rel="shortcut icon" href="/img/logo.png" type="image/x-icon">
    @vite('resources/css/app.css')
</head>

<body class="overflow-x-hidden">
    @yield('content')
</body>

</html>
