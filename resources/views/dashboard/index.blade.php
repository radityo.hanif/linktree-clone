@extends('layouts.dashboard')
@section('content')
    <div class="grid gap-[100px]">
        <div class="grid gap-5">
            <h1 class="text-2xl font-bold text-blue-500">
                Dashboard
            </h1>
            <div class="grid grid-cols-4 gap-5">
                @foreach ($statistics as $one)
                    <div class="flex rounded-xl" style="box-shadow: rgba(59, 130, 246, 0.3) 0px 10px 50px;">
                        <div class="grid gap-0 p-4 px-6 w-[100%]">
                            <p class="font-normal">{{ $one['key'] }}</p>
                            <h1 class="text-3xl font-semibold text-blue-500">{{ $one['value'] }}</h1>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="grid gap-3">
            <h1 class="text-2xl font-bold text-blue-500">
                Visitor
            </h1>
            <div class="p-5 rounded-xl" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;">
                <table class="table compact hover row-border" id="datatables-visitor"></table>
            </div>
        </div>

        <div class="grid gap-3">
            <h1 class="text-2xl font-bold text-blue-500">
                Visitor Log
            </h1>
            <div class="p-5 rounded-xl" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;">
                <table class="table compact hover row-border" id="datatables-visitor-log"></table>
            </div>
        </div>

        <div class="grid grid-cols-2 items-start gap-10">
            <div class="grid gap-3">
                <h1 class="text-2xl font-bold text-blue-500">
                    Most clicked Buttons
                </h1>
                <canvas id="chart-most-clicked-button"></canvas>
            </div>
            <div class="grid gap-3">
                <h1 class="text-2xl font-bold text-blue-500">
                    Button Click
                </h1>
                <div class="p-5 rounded-xl" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;">
                    <table class="table compact hover row-border" id="datatables-button-click"></table>
                </div>
            </div>
        </div>
    </div>

    {{-- Chart: Most Clicked Button --}}
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            fetchDataAndRenderChart();
        });

        async function fetchDataAndRenderChart() {
            const response = await fetch('/api/dashboard/button-click/pie-chart', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({}),
            });

            const data = await response.json();

            if (response.ok) {
                renderChart(data);
            } else {
                console.error('Failed to fetch data:', data);
            }
        }

        function renderChart(data) {
            console.log(data.labels);
            console.log(data.values);
            const ctx = document.getElementById('chart-most-clicked-button').getContext('2d');
            const myPieChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: data.labels,
                    datasets: [{
                        label: 'My Pie Chart',
                        data: data.values,
                    }],
                    hoverOffset: 4
                },
            });
        }
    </script>

    {{-- Dashboard Datatables --}}
    <script>
        $('#datatables-visitor').DataTable({
            ajax: {
                method: 'POST',
                url: '/api/dashboard/visitor',
            },
            columnDefs: [{
                targets: -1,
                className: 'dt-body-right'
            }],
            columns: [{
                    width: '5%',
                    title: 'ID',
                    data: 'id'
                },
                {
                    title: 'Date',
                    data: 'created_at'
                },
                {
                    title: 'Visitor',
                    data: 'visitor_id'
                },
                {
                    title: 'Country of Origin',
                    data: 'country_of_origin'
                },
                {
                    title: 'IP Address',
                    data: 'ip_address'
                },
            ]
        });

        $('#datatables-visitor-log').DataTable({
            ajax: {
                method: 'POST',
                url: '/api/dashboard/visitor-log',
            },
            columnDefs: [{
                targets: -1,
                className: 'dt-body-right'
            }],
            columns: [{
                    width: '5%',
                    title: 'ID',
                    data: 'id'
                },
                {
                    title: 'Date',
                    data: 'created_at'
                },
                {
                    title: 'Visitor',
                    data: 'visitor.visitor_id'
                },
                {
                    title: 'Country of Origin',
                    data: 'visitor.country_of_origin'
                },
                {
                    title: 'Time Spent (second)',
                    data: 'time_spent'
                },
                {
                    title: 'Browser Information',
                    data: 'browser_information'
                },
                {
                    title: 'Device Information',
                    data: 'device_information'
                },
            ]
        });

        $('#datatables-button-click').DataTable({
            ajax: {
                method: 'POST',
                url: '/api/dashboard/button-click',
            },
            columnDefs: [{
                targets: -1,
                className: 'dt-body-right'
            }],
            columns: [{
                    width: '5%',
                    title: 'ID',
                    data: 'id'
                },
                {
                    title: 'Date',
                    data: 'created_at'
                },
                {
                    title: 'Visitor',
                    data: 'visitor.visitor_id'
                },
                {
                    title: 'Button',
                    data: 'button_name'
                },
            ]
        });
    </script>
@endsection
