@extends('layouts.dashboard')
@section('content')
    <form action="{{ route('action.setting.update') }}" method="post">
        @csrf
        <div class="grid gap-5">
            <div class="flex justify-between items-center">
                <h1 class="text-2xl font-bold text-blue-500">
                    Themes
                </h1>
            </div>
            <p>
                Select the theme that you want to use as the main theme on the website. The theme you choose will affect the
                overall appearance of your website.
            </p>
            <label class="form-control w-full max-w-xs">
                <div class="label">
                    <span class="label-text">Main Theme</span>
                </div>
                <input type="hidden" name="key" value="main_theme">
                <select name="value" class="input input-bordered w-full max-w-xs">
                    <option value="theme_1" {{ $value == 'theme_1' ? 'selected' : '' }}>Theme 1</option>
                    <option value="theme_2" {{ $value == 'theme_2' ? 'selected' : '' }}>Theme 2</option>
                </select>
            </label>
            <button type="submit" class="btn btn-grow bg-blue-500 text-white hover:bg-blue-600 max-w-xs">
                SUBMIT
            </button>
        </div>
    </form>
@endsection
