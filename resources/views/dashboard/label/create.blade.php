@extends('layouts.dashboard')
@section('content')
    <form action="{{ route('action.label.create') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="grid gap-5 md:w-[50%] w-[100%]">
            <h1 class="text-2xl font-bold text-blue-500">
                Create Label
            </h1>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Label Name</span>
                </div>
                <input type="text" placeholder="Type here" name="name" class="input input-bordered input-sm w-full" />
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Label Description</span>
                </div>
                <textarea placeholder="Type here" name="description" class="textarea textarea-bordered textarea-sm w-full"></textarea>
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Label Text Color</span>
                </div>
                <input type="color" placeholder="Type here" name="title_color"
                    class="input input-bordered input-sm w-full" />
            </label>
            <label class="form-control w-full">
                <div class="label">
                    <span class="label-text">Label Icon</span>
                </div>
                <input type="file" name="icon" class="file-input file-input-bordered file-input-sm w-full" />
                <div class="label">
                    <span class="label-text text-[#a0a0a0]">
                        It is recommended to use the .png format
                    </span>
                </div>
            </label>
            <button type="submit" class="btn btn-grow bg-blue-500 text-white hover:bg-blue-600">
                SUBMIT
            </button>
        </div>
    </form>
@endsection
