@extends('layouts.dashboard')
@section('content')
    <div class="grid gap-5">
        <div class="flex justify-between items-center ">
            <h1 class="text-2xl font-bold text-blue-500">
                Labels
            </h1>
            <a href="/admin/dashboard/label/create">
                <button class="btn btn-grow bg-blue-500 text-white hover:bg-blue-600">
                    Create New
                </button>
            </a>
        </div>
        <p>
            Set the label data here to change the website content
        </p>
        <div class="p-5 rounded-xl" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;">
            <table class="table compact hover row-border" id="datatables">
            </table>
        </div>
    </div>
    <script>
        const deleteAction = () => {
            $(".btn-delete").click(function(e) {
                const id = $(this).attr("data-id");
                Swal.fire({
                    title: "Do you want to delete this data?",
                    showCancelButton: true,
                    confirmButtonText: "Yes",
                    confirmButtonColor: "#00ABE7"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "DELETE",
                            url: "/api/label",
                            data: {
                                "id": id
                            },
                            success: function(response) {
                                Swal.fire("Success delete data", "", "success")
                                    .then((result) => {
                                        window.location.reload()
                                    });
                            }
                        });
                    }
                });
            });
        }

        $('#datatables').DataTable({
            ajax: {
                method: 'POST',
                url: '/api/label',
            },
            initComplete: function() {
                deleteAction()
            },
            columns: [{
                    title: 'Title',
                    data: 'title'
                },
                {
                    title: 'Description',
                    data: 'description'
                },
                {
                    title: 'Action',
                    data: function(data) {
                        return `
                        <button data-id="${data.id}" class="btn btn-grow bg-red-400 hover:bg-red-500 btn-delete text-white">
                            Delete
                        </button>
                        `
                    }
                },
            ]
        });
    </script>
@endsection
