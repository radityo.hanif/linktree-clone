@extends('layouts.public')
@section('content')
    <div class="flex justify-center h-min-[100vh] bg-gradient-to-br from-[#3d55dc] to-[#001b46]">
        <div class="container">
            <div class="grid gap-[100px]">
                <div id="jumbotron" class="h-screen">
                    <div class="absolute inset-0 bg-cover bg-center bg-fixed z-10" style="background-image: url('https://images.unsplash.com/photo-1558591710-4b4a1ae0f04d?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')">
                    </div>
                    <div class="absolute inset-0 bg-gradient-to-t from-[#fff] via-[#fff] to-transparent flex justify-center items-center z-30">
                        <div class="grid gap-10 p-8">
                            <div class="flex justify-center">
                                <div class="avatar">
                                    <div class="w-[100%] h-[100px] rounded-full">
                                        <img src="/img/logo-alt.png" />
                                    </div>
                                </div>
                            </div>
                            <div class="grid gap-5">
                                <h1 style="text-shadow: rgba(255, 255, 255, 0.8) 0px 0px 30px;" class="text-5xl font-normal text-center text-[#000] animate-pulse">
                                    Smartgenix
                                </h1>
                                <p class="text-lg text-center">
                                    Smartgenix is a genuinely innovative company within its industry.
                                </p>
                                <div id="scroll-down">
                                    <span class="arrow-down">
                                    </span>
                                    <span id="scroll-title">
                                        Scroll down
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="download-our-apps">
                    <div class="grid xl:grid-cols-2 grid-cols-1 gap-10 justify-start items-center">
                        <div class="grid gap-5">
                            <div class="mockup-phone shadow-2xl" id="mockup-phone">
                                <div class="camera"></div>
                                <div class="display">
                                    <div class="artboard artboard-demo phone-1">
                                        <div class="bg-[#fff] h-[100%] w-[100%] flex justify-center items-center text-white">
                                            <div class="grid gap-0">
                                                <div class="flex justify-center">
                                                    <div class="avatar">
                                                        <div class="w-[100%] h-[100px] rounded-full">
                                                            <img src="/img/logo.png" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid gap-5">
                                                    <h1 style="text-shadow: rgba(255, 255, 255, 0.8) 0px 0px 30px;" class="text-2xl font-normal text-center text-[#00ABE7]">
                                                        Smartgenix
                                                    </h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid gap-5 xl:text-start text-center">
                            <h1 class="text-4xl text-white">
                                Download our Application
                            </h1>
                            <p class="text-white">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam eos sapiente expedita
                                facilis animi dolor praesentium dolores beatae, et vero corporis omnis vitae error incidunt
                                ratione officia ipsa. Eaque, ratione?
                            </p>
                            <div class="flex gap-10 xl:justify-start justify-center">
                                <a id="button-download-apps-store" href="https://play.google.com/store" target="" class="market-btn apple-btn button-log" role="button">
                                    <span class="market-button-subtitle">Download on the</span>
                                    <span class="market-button-title">App Store</span>
                                </a>
                                <a id="button-download-google-play" href="https://play.google.com/store" target="" class="market-btn google-btn button-log" role="button">
                                    <span class="market-button-subtitle">Download on the</span>
                                    <span class="market-button-title">Google Play</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="visit-our-web" class="grid gap-5">
                    <div class="flex justify-center">
                        <a href="https://smartgenix.co.uk/" class="relative btn btn-wide btn-lg button-log rounded-2xl btn-outline text-white border-white" id="button-visit-our-website">
                            <span class="animate-ping absolute h-full w-full rounded-2xl bg-sky-200 opacity-75"></span>
                            Visit Our Website
                        </a>
                    </div>
                </div>

                <div id="login-as-a" class="grid gap-10">
                    <div class="grid gap-3 text-center text-white">
                        <h1 class="text-3xl">
                            Login as a
                        </h1>
                        <div class="flex justify-center">
                            <p class="xl:w-[50%] w-[100%] ">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                    <div class="flex justify-center">
                        <div class="grid grid-cols-2 gap-10 w-[100%] xl:w-[50%]">
                            @foreach ($labels as $label)
                                <div class="flex justify-start items-center gap-2">
                                    <button class="button-92 button-log text-lg" id="button-{{ $label['slug'] }}" onclick="modal_{{ $label['slug'] }}.showModal()">
                                        {{ $label['title'] }}
                                    </button>
                                </div>
                                <dialog id="modal_{{ $label['slug'] }}" class="modal h-screen absolute w-screen bg-white bg-opacity-10 z-[10] backdrop-blur">
                                    <div class="modal-box relative overflow-visible">
                                        <div>
                                            <h2 class="text-[#00ABE7] text-3xl font-semibold">{{ $label['title'] }}</h2>
                                            <p class="mt-2 text-gray-600">{{ $label['description'] }}</p>
                                        </div>
                                    </div>
                                    <form method="dialog" class="modal-backdrop">
                                        <button>close</button>
                                    </form>
                                </dialog>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div id="social-media" class="grid gap-10">
                    <div class="grid gap-3 text-center text-white">
                        <h1 class="text-3xl">
                            Follow Our Social Media
                        </h1>
                        <div class="flex justify-center">
                            <p class="xl:w-[50%] w-[100%] ">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                    <div class="flex gap-10 justify-center" id="social-media-container">
                        <div class="flex justify-center">
                            <a href="https://www.facebook.com/Smartgenix" id="button-facebook" class="button-log btn rounded-full border-none text-lg text-white bg-[#1877F2] hover:bg-[#1877F2]">
                                <i class="bi bi-facebook"></i>
                            </a>
                        </div>
                        <div class="flex justify-center">
                            <a href="https://www.instagram.com/smartgenix_official/" id="button-instagram" class="button-log btn rounded-full border-none text-lg text-white bg-gradient-to-br from-[#833AB4] via-[#E1306C] to-[#FD9426]">
                                <i class="bi bi-instagram"></i>
                            </a>
                        </div>
                        <div class="flex justify-center">
                            <a href="#" id="button-snapchat" class="button-log btn rounded-full border-none text-lg text-black bg-[#FFFC00] hover:bg-[#FFFC00]">
                                <i class="bi bi-snapchat"></i>
                            </a>
                        </div>
                        <div class="flex justify-center">
                            <a href="https://twitter.com/Smartgenix_ltd" id="button-twitter" class="button-log btn rounded-full border-none text-lg text-white bg-[#000000] hover:bg-[#000000]">
                                <i class="bi bi-twitter-x"></i>
                            </a>
                        </div>
                        <div class="flex justify-center">
                            <a href="https://www.youtube.com/@smartgenix" id="button-youtube" class="button-log btn rounded-full border-none text-lg text-white bg-[#FF0000] hover:bg-[#FF0000]">
                                <i class="bi bi-youtube"></i>
                            </a>
                        </div>
                        <div class="flex justify-center">
                            <a href="https://www.linkedin.com/company/92940294/" id="button-linkedin" class="button-log btn rounded-full border-none text-lg text-white bg-[#0A66C2] hover:bg-[#0A66C2]">
                                <i class="bi bi-linkedin"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div id="footer" class="grid gap-10">
                    <div class="flex justify-center gap-10">
                        <div class="grid gap-3">
                            <h1 class="text-center text-white">Join the Team</h1>
                            <div class="flex justify-center">
                                <button class="button-92 button-log" id="button-view-jobs">
                                    View Jobs
                                </button>
                            </div>
                        </div>
                        <div class="grid gap-3">
                            <h1 class="text-center text-white">Contact Us</h1>
                            <div class="flex justify-center">
                                <button class="button-92 button-log" id="button-view-jobs">
                                    Support
                                </button>
                            </div>
                        </div>
                    </div>
                    <p class="text-center font-normal text-white mb-5">
                        ©Copyright 2024 Smartgenix
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
