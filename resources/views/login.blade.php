@extends('layouts.default')
@section('content')
    <div class="circle w-[10%] h-[400px] bg-[#a2e6ff] rounded-[100%] absolute z-1 blur-[90px]"></div>
    <div class="circle w-[10%] h-[400px] bg-[#b1ffae] rounded-[100%] absolute z-1 blur-[90px] right-1"></div>
    <div class="flex h-screen justify-center items-center bg-gradient-to-br from-[#d4f4ff] to-[#d8fffc]">
        <div class="container xl:w-[30vw] lg:w-[50vw]">
            <div class="xl:p-[50px] p-[30px] md:p-[100px] bg-white rounded-xl backdrop-blur-xl bg-opacity-60"
                style="box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px">

                <form action="{{ route('action.login') }}" method="post">
                    @csrf
                    <div class="grid gap-5">
                        <div class="text-center">
                            <div class="flex justify-center">
                                <div class="avatar">
                                    <div class="w-[100%] h-[50px] rounded-full opacity-80">
                                        <img src="/img/logo.png" />
                                    </div>
                                </div>
                            </div>
                            <h1 style="text-shadow: rgba(0, 171, 231, 0.8) 0px 0px 30px;"
                                class="text-3xl font-normal text-center text-[#00ABE7]">
                                Smartgenix
                            </h1>
                            <p class="font-light">
                                Please sign in here
                            </p>
                        </div>

                        @include('components.alert')

                        <label class="input flex items-center gap-2">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor"
                                class="w-4 h-4 opacity-70">
                                <path
                                    d="M2.5 3A1.5 1.5 0 0 0 1 4.5v.793c.026.009.051.02.076.032L7.674 8.51c.206.1.446.1.652 0l6.598-3.185A.755.755 0 0 1 15 5.293V4.5A1.5 1.5 0 0 0 13.5 3h-11Z" />
                                <path
                                    d="M15 6.954 8.978 9.86a2.25 2.25 0 0 1-1.956 0L1 6.954V11.5A1.5 1.5 0 0 0 2.5 13h11a1.5 1.5 0 0 0 1.5-1.5V6.954Z" />
                            </svg>
                            <input type="text" class="grow" name="email" placeholder="Email" />
                        </label>

                        <label class="input flex items-center gap-2">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor"
                                class="w-4 h-4 opacity-70">
                                <path fill-rule="evenodd"
                                    d="M14 6a4 4 0 0 1-4.899 3.899l-1.955 1.955a.5.5 0 0 1-.353.146H5v1.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-2.293a.5.5 0 0 1 .146-.353l3.955-3.955A4 4 0 1 1 14 6Zm-4-2a.75.75 0 0 0 0 1.5.5.5 0 0 1 .5.5.75.75 0 0 0 1.5 0 2 2 0 0 0-2-2Z"
                                    clip-rule="evenodd" />
                            </svg>
                            <input type="password" class="grow" name="password" placeholder="Password" />
                        </label>

                        <button type="submit" class="btn btn-grow bg-[#00ABE7] text-white hover:bg-[#00394D]">
                            LOGIN
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
