const responsiveHandler = (windowWidth) => {
    if (windowWidth <= 768) {
        console.log("Mobile device detected!");
        $("#social-media-container").attr(
            "class",
            "grid gap-10 justify-center"
        );
        // $("#mockup-phone").attr("class", "mockup-phone shadow-2xl hidden");
    } else {
        console.log("Desktop device detected!");
        $("#social-media-container").attr(
            "class",
            "flex gap-10 justify-center"
        );
        // $("#mockup-phone").attr("class", "mockup-phone shadow-2xl");
    }
};

$(window).on("resize", function () {
    const windowWidth = $(this).width();
    responsiveHandler(windowWidth);
});

$(document).ready(function () {
    const windowWidth = $(window).width();
    responsiveHandler(windowWidth);
});
