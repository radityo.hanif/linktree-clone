$(document).ready(function () {
    localStorage.setItem("click_logs", JSON.stringify([]));
    $(".button-log").click(function (e) {
        const buttonId = $(this).attr("id");
        const clickLogs = JSON.parse(localStorage.getItem("click_logs"));
        clickLogs.push(buttonId);
        localStorage.setItem("click_logs", JSON.stringify(clickLogs));
    });
});

const startTime = new Date().getTime();
const myEvent = window.attachEvent || window.addEventListener;
const chkevent = window.attachEvent ? "onbeforeunload" : "beforeunload"; /// make IE7, IE8 compitable

myEvent(chkevent, function (e) {
    sendDataToPHP(startTime);
    return confirmationMessage;
});

function milisecondToSecond(milliseconds) {
    const seconds = Math.floor(milliseconds / 1000);
    return seconds;
}

function sendDataToPHP(startTime) {
    const endTime = new Date().getTime();
    const timeSpent = milisecondToSecond(endTime - startTime);
    const url = "api/visitor/log";
    const formData = new FormData();

    formData.append("click_logs", localStorage.getItem("click_logs"));
    formData.append("time_spent", timeSpent);

    fetch(url, {
        method: "POST",
        body: formData,
    })
        .then((response) => response.json())
        .then((data) => {
            console.log("Response:", data);
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}
