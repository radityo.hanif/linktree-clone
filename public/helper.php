<?php
    chdir('..'); // Change to the parent directory
    $output = [];
    $return_var = 0;
    exec('/usr/bin/php81 linktree.smartgenix.co.uk/artisan storage:link', $output, $return_var);

    // Check if the command was executed successfully
    if ($return_var === 0) {
        var_dump($output);
        echo "Storage link created successfully.";
    } else {
        var_dump($output);
        echo "Error creating storage link.";
    }
?>
