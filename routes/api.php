<?php

use App\Http\Controllers\ClickLogsController;
use App\Http\Controllers\LabelController;
use App\Http\Controllers\SetupDataController;
use App\Http\Controllers\VisitorLogsController;
use App\Http\Controllers\VisitorsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the 'api' middleware group. Make something great!
|
*/

Route::middleware(['throttle:public'])->group(function () {
    Route::get('/setup-data', [SetupDataController::class, 'init']);
    Route::get('/check/env', [SetupDataController::class, 'getEnv']);
    Route::post('/visitor/log', [VisitorLogsController::class, 'create']);
});

Route::middleware(['throttle:public'])->prefix('label')->group(function () {
    Route::post('/', [LabelController::class, 'read']);
    Route::delete('/', [LabelController::class, 'delete']);
});

Route::middleware(['throttle:public'])->prefix('dashboard')->group(function () {
    Route::post('/visitor-log', [VisitorLogsController::class, 'read']);
    Route::post('/visitor', [VisitorsController::class, 'read']);
    Route::post('/button-click', [ClickLogsController::class, 'read']);
    Route::post('/button-click/pie-chart', [ClickLogsController::class, 'pieChart']);
});
