<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LabelController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::prefix('action')->group(function () {
    Route::post('login', [AuthController::class, 'login'])->name('action.login');
    Route::post('label/create', [LabelController::class, 'create'])->name('action.label.create');
    Route::post('setting/update', [SettingController::class, 'update'])->name('action.setting.update');
});
Route::get('/', [HomeController::class, 'index']);
Route::prefix('admin')->group(function () {
    Route::get('login', function () {
        return view('login');
    })->name('login');

    Route::get('logout', function () {
        session()->invalidate();
        return redirect('/admin/login');
    });

    Route::prefix('dashboard')->middleware(['check.session'])->group(function () {
        Route::get('/', [DashboardController::class, 'index']);
        Route::prefix('label')->group(function () {
            Route::get('/', function () {
                return view('dashboard.label.index');
            });
            Route::get('create', function () {
                return view('dashboard.label.create');
            });
            Route::get('detail', function () {
                return view('dashboard.label.detail');
            });
        });
        Route::prefix('themes')->group(function () {
            Route::get('/', [SettingController::class, 'themes']);
        });
    });
});
