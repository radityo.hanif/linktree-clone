# HOW TO SETUP

-- config .env
CACHE_DRIVER=array
ADMIN_DEFAULT_EMAIL=[.....]
ADMIN_DEFAULT_PASSWORD=[.....]

-- install apps
composer install
npm install
npm run build

-- install database
php artisan migrate

-- other things todo
php artisan storage:link
php artisan vendor:publish --provider="Torann\GeoIP\GeoIPServiceProvider" --tag=config
php artisan geoip:update
go to https://[your_apps_base_url]/api/setup-data

-- admin page
go to https://[your_apps_base_url]/admin/login
